/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POC;

import database.Database;
import java.sql.*;
import model.Product;

/**
 *
 * @author Kanny
 */
public class TestInsertProduct {
    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "INSERT INTO product (name,price)VALUES (?,?)";
            PreparedStatement stmt = con.prepareStatement(sql);
            Product product = new Product(-1,"Cappuchino",45);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            int row = stmt.executeUpdate();            
            ResultSet result = stmt.getGeneratedKeys();
            System.out.println(result.getInt(1));
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
        
        
    }
}

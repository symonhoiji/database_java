/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POC;

import database.Database;
import java.sql.*;
import model.Product;

/**
 *
 * @author Kanny
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
 
        try {
            
            String sql = "SELECT * FROM product";
            Statement stmt = con.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
                
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id,name,price);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        
        db.close();
    }
}

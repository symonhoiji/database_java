/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POC;
import database.Database;
import java.sql.*;
import model.Product;

/**
 *
 * @author Kanny
 */
public class TestUpdateProduct {
    public static void main(String[] args) {
        Connection con = null;
        Database db = Database.getInstance();
        con = db.getConnection();
        
        try {
            String sql = "UPDATE product SET  name = ?,price = ? WHERE id = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            Product product = new Product(7,"Cappuchino3",40);
            stmt.setString(1, product.getName());
            stmt.setDouble(2, product.getPrice());
            stmt.setInt(3, product.getId());
            int row = stmt.executeUpdate();            
            
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        db.close();
    }
}
